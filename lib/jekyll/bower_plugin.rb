require 'uri'

module Jekyll
	class Bower
		def initialize()
			puts "Checking if NPM is available\n"
			fail unless system('which npm')
			puts "Great, NPM is available\n"
			puts "Checkin if Bower is available\n"
			unless system('which bower')
				puts "Bower not available\n"
				puts "Installing bower\n"
				system('npm install -g bower')
				puts "Installation completed\n"
			else
				puts "Great, Bower is available\n"
			end
		end

		def resolve(site)
			if File.exists?("bower.json")
				puts "Resolving dependencies with bower\n"
				unless system('bower install')
					puts "Looks like bower is run by a sudo user\n"
					puts "Trying with --allow-root\n"
					system('bower install --allow-root')
				end
				puts "Bower dependencies resolved successfully\n"
			else
				if site.config['bower']
					puts "Resolving bower individually configured in config" + "\n"
					site.config['bower'].each do |name, library| 
						unless File.exists?('bower_components' + File::SEPARATOR + name)
							bower_command = 'bower install ' + library
							unless system(bower_command)
								puts "Trying resolving bower with allow-root" + "\n"
								system(bower_command + ' --allow-root')
							end

							if library =~ URI::regexp
								puts "Identified bower downloaded dependency is a URL" + "\n"
								puts "Performing rename activity if possible" + "\n"
								uri = URI.parse(library)
								filename = File.basename(uri.path, ".*")
								bowerOld = 'bower_components' + File::SEPARATOR + filename
								bowerNew = 'bower_components' + File::SEPARATOR + name
								unless File.exists?(bowerNew)
									File.rename(bowerOld, bowerNew)
									puts "Renamed bower ("+filename+") library to " + name + "\n"
								end
							end
							puts "Downloaded bower dependency: " + name + "\n"
						else
							puts "Bower dependency exists, skipping download\n"
						end
					end
				end
			end
		end
	end
end