# Generate script code for Google Analytics with Liquid Tag
require 'jekyll'
require 'jekyll/bower_plugin'
require 'jekyll/plugin_version'

# Register Jekyll Site Post Read hook of Bower plugin
Jekyll::Hooks.register :site, :after_init do |jekyll| # jekyll here acts as site global object
	JekyllBower = Jekyll::Bower.new()
	JekyllBower.resolve(jekyll)
	puts "Reading newly generated bower components"
	jekyll.reset()
	puts "Jekyll site reset done"
	jekyll.setup()
	puts "Jekyll site setup done"
	jekyll.read()
	puts "Jekyll site read done"
	puts "Looks like everything is ready"
end